import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";

export const useUserStore = defineStore("user", () => {
  const dialog = ref(false);
  let lastId = 4;
  const editedUser = ref<User>({ id: -1, login: "", name: "", password: "" });
  const users = ref<User[]>([
    { id: 1, login: "admin", name: "Adiminstrator", password: "Pass@1234" },
    { id: 2, login: "user1", name: "User 1", password: "Pass@1234" },
    { id: 3, login: "user2", name: "User 2", password: "Pass@1234" },
  ]);

  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => item.id === id);
    users.value.splice(index, 1);
  };
  const saveUser = () => {
    if (editedUser.value.id < 0) {
      editedUser.value.id = lastId++;
      users.value.push(editedUser.value);
    } else {
      const index = users.value.findIndex(
        (item) => item.id === editedUser.value.id
      );
      users.value[index] = editedUser.value;
    }

    dialog.value = false;
    clear();
  };
  const clear = () => {
    editedUser.value = { id: -1, login: "", name: "", password: "" };
  };
  const editUser = (user: User) => {
    editedUser.value = { ...user };
    dialog.value = true;
  };
  return { users, deleteUser, dialog, editedUser, clear, saveUser, editUser };
});
